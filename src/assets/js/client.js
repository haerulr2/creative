const publicVapidKey = 'BHuWUmBJHOIv7HXgIT08Fi7GVp2ydw1fgLmleggspptkbsJbL5Y3Q9FTvuJ4m-HVnmGtmrfMmzhXsB5q80H8R20';


if ('serviceWorker' in navigator) {
	run().then(res => console.log(res)).catch(error => console.error(error));
}

async function run() {
	const registration = await navigator.serviceWorker.register('./assets/js/worker.js');
	const subscription = await registration.pushManager.subscribe({
		userVisibleOnly: true,
		applicationServerKey: urlBase64ToUint8Array(publicVapidKey)
	});
	localStorage.setItem('subscription',JSON.stringify(subscription))
}

// Boilerplate borrowed from https://www.npmjs.com/package/web-push#using-vapid-key-for-applicationserverkey
function urlBase64ToUint8Array(base64String) {
	const padding = '='.repeat((4 - base64String.length % 4) % 4);
	const base64 = (base64String + padding)
		.replace(/\-/g, '+')
		.replace(/_/g, '/');

	const rawData = window.atob(base64);
	const outputArray = new Uint8Array(rawData.length);

	for (let i = 0; i < rawData.length; ++i) {
		outputArray[i] = rawData.charCodeAt(i);
	}
	return outputArray;
}
