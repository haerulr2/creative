let slug = '';

self.addEventListener('push', ev => {
    const resp = ev.data.json();
    let data = JSON.parse(resp);
    console.log(data);
    slug = data.slug;
    self.registration.showNotification(data.title, {
        body: data.body,
        icon: data.icon ? data.icon : '/assets/img/favicon.png',
        image: data.image,
        badge: '/assets/img/favicon.png',
    });
});

self.addEventListener('notificationclick', function (event) {
    event.notification.close();
    let get_host = event.currentTarget.location.host;
    let set_url = "https://" + get_host + '/' + slug;
    clients.openWindow(set_url);
});