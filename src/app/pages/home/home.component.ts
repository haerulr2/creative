import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void { }
  
  loginForm = new FormGroup({
    name: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    phone: new FormControl('', Validators.required),
    message: new FormControl('', Validators.required)
  })

  get name(){ return this.loginForm.get('name')}
  get email(){ return this.loginForm.get('email')}
  get phone(){ return this.loginForm.get('phone')}
  get message(){ return this.loginForm.get('message')}

  checkName: boolean = false;
  checkEmail: boolean = false;
  checkPhone: boolean = false;
  checkMessage: boolean = false;
  checkBtn: boolean = false;
  validator(){
    if (this.name?.invalid && this.name.touched)
    { this.checkName = true; } else { this.checkName = false;}
    if (this.email?.invalid && this.email.touched)
    { this.checkEmail = true; } else { this.checkEmail = false;}
    if (this.phone?.invalid && this.phone.touched)
    { this.checkPhone = true; } else { this.checkPhone = false;}
    if (this.message?.invalid && this.message.touched)
    { this.checkMessage = true; } else { this.checkMessage = false;}
    if (this.name?.valid && this.email?.valid && this.phone?.valid && this.message?.valid) 
    { this.checkBtn = true; } else { this.checkBtn = false;}
  }
}
