import { Component, HostListener, OnInit } from '@angular/core';
declare const $:any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  bgScroll: boolean = false;

  constructor() { }

  ngOnInit(): void {}
  
  @HostListener(`window:scroll`,[`$event`])
  onScroll(){
    if (window.scrollY > 1) {
      this.bgScroll = true;
    } else {
      this.bgScroll = false;
    }
  } 
}